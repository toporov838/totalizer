import React, { useState } from "react";

import styles from "./Counter.module.css"

let a = 0
function Counter() {
  const [count, setCount] = useState(a)
  const handleClickPlus = () => {
    setCount(a += 1)
  }
  const handleClickMinus = () => {
    setCount(a -= 1)
  }
  return <>
    <div className={styles.flash}>
      <div className={styles.wrapper}><div className={styles.num}>{count}</div> <button onClick={handleClickPlus} className={styles.button}>+</button>
        <button onClick={handleClickMinus} className={styles.button}>-</button></div>
    </div>
  </>
}
export default Counter